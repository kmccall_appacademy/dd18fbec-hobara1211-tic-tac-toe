class Board

  def  initialize(grid=nil)
    if grid == nil
    @grid =
    [[nil, nil, nil],
     [nil, nil, nil],
     [nil, nil, nil]]
    else
      @grid = grid
    end
  end

  def grid
    @grid
  end

  def place_mark(pos, mark)
    if !@grid[pos.first][pos.last].nil?
      raise "error"
    else
      @grid[pos.first][pos.last] = mark
    end
  end

  def empty?(pos)
    @grid[pos.first][pos.last] == nil ? true : false
  end

  def winner
    # case1 and case2 check by row and column.
    case1 = @grid.select { |row| row.count(row[0]) == 3 unless row[0] == nil }
    case2 = @grid.transpose.select { |row| row.count(row[0]) == 3 unless row[0] == nil }
    # case3 and case4 check by diagonal.
    case3 = [@grid[0][0], @grid[1][1], @grid[2][2]]
    case4 = [@grid[0][2], @grid[1][1], @grid[2][0]]
    if !case1.empty?
      return case1[0][0]
    elsif !case2.empty?
      return case2[0][0]
    elsif case3.count(case3[0]) == 3 && case3[0] != nil
      return case3[0]
    elsif case4.count(case4[0]) == 3 && case4[0] != nil
      return case4[0]
    else
      return nil
    end
  end

  def over?
    if @grid.flatten.none? { |el| el == nil }
      true
    elsif self.winner == nil
      false
    else
      true
    end
  end

  def [](a)
    @grid[a]
  end

end
