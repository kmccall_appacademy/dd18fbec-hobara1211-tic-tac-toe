class HumanPlayer

  # attr_reader :name, :board
  # attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def name
    @name
  end

  def get_move
    puts "where do you want to mark?"
    input = gets.chomp
    [input.split(",").first.to_i, input.split(",").last.to_i]
  end

  def display(board)
    to_be_printed = []
    (0..2).each do |i|
      (0..2).each do |j|
        if board[i][j] == nil
          to_be_printed << " "
        else
          to_be_printed << board[i][j].to_s
        end
      end
      print to_be_printed.join("|")
      print "\n"
      to_be_printed = []
    end
  end

end
