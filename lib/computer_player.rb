class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def name
    @name
  end

  def board
    @board
  end

  def display(board)
    @board = board
  end

  def get_move
    move_option = []
    (0..2).each do |i|
      (0..2).each do |j|
        move_option << [i, j] if @board[i][j] == nil
      end
    end
    move_option.each do |move|
      @board[move.first][move.last] = mark
      if @board.winner == mark
        @board[move.first][move.last] = nil
        return move
      else
        @board[move.first][move.last] = nil
      end
    end
    move_option[rand(move_option.length - 1)]
  end

end
