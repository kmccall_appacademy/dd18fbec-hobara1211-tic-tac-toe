require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :player_one, :player_two, :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    player_one.mark = :X
    player_two.mark = :O
    @current_player = player_one
  end

  def play_turn
    move = @player_one.get_move
    mark = @player_one.mark
    @board.place_mark(move, mark)
    switch_players!
  end

  def switch_players!
    if self.current_player == @player_one
      self.current_player = @player_two
    else
      self.current_player = @player_one
    end
  end

end
